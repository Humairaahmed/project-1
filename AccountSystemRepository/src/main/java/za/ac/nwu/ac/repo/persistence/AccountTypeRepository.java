package za.ac.nwu.ac.repo.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import za.ac.nwu.ac.domain.persistence.AccountType;

@Repository
public interface AccountTypeRepository extends JpaRepository<AccountType, Long> {



    @Query(value = "UPDATE  " +
            "ACCOUNT_TRANSACTION" +
            "SET" +
            "WHERE" +
            "amount =  " + amount)
    AccountType addMiles(String amount);

    @Query(value = "UPDATE  " +
            "ACCOUNT_TRANSACTION" +
            "SET" +
            "WHERE" +
            "amount =  " + amount)
    AccountType subtractMiles(String amount);

    @Query(value = "SELECT  " +
            "amount" +
            "FROM" +
            "ACCOUNT_TRANSACTION")
    AccountType display();
}
