package za.ac.nwu.ac.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import za.ac.nwu.ac.domain.persistence.AccountType;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@ApiModel(value = "AccountType",
        description = "A DTO that represents the AccountType")
public class AccountTypeDto implements Serializable {
    private String mnemoic;
    private String accountTypeName;
    private LocalDate creationDate;

    public AccountTypeDto() {
    }

    public AccountTypeDto(String mnemoic, String accountTypeName, LocalDate creationDate) {
        this.mnemoic = mnemoic;
        this.accountTypeName = accountTypeName;
        this.creationDate = creationDate;
    }

    public AccountTypeDto(AccountType accountType){
        this.setAccountTypeName(accountType.getAccountTypeName());
        this. setCreationDate(accountType.getCreationDate());
        this.setMnemoic(accountType.getMnemonic());
    }

    @ApiModelProperty(position = 1,
    value = "AccountType Menmoic",
    name = "Mnemoic",
    notes = "Uniquely identifies the account type",
    dataType = "java.lang.String",
    example = "MILES",
    required = true)
    public String getMnemoic() {
        return mnemoic;
    }

    public void setMnemoic(String mnemoic) {
        this.mnemoic = mnemoic;
    }

    @ApiModelProperty(position = 2,
    value = "AccountTypename",
    name = "name",
    notes = "The name of the AccountType",
    dataType = "java.lang.String",
    example = "Miles",
    allowEmptyValue = false,
    required = true)
    public String getAccountTypeName() {
        return accountTypeName;
    }

    public void setAccountTypeName(String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }

    @ApiModelProperty(position = 3,
    value = "AccountType Creation Date",
    name = "CreationDate",
    notes = "This is the date on which the AccountType was created",
    dataType = "java.lang.String",
    example = "2021-01-01",
    allowEmptyValue = true,
    required = false)
    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountTypeDto that = (AccountTypeDto) o;
        return  Objects.equals(mnemoic, that.mnemoic) && Objects.equals(accountTypeName, that.accountTypeName) && Objects.equals(creationDate, that.creationDate);
    }

    @JsonIgnore
    public AccountType getAccountType(){
        return new AccountType(getMnemoic(),getAccountTypeName(),getCreationDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(mnemoic, accountTypeName, creationDate);
    }

    @Override
    public String toString() {
        return "AccountTypeDto{" +
                "mnemoic='" + mnemoic + '\'' +
                ", accountTypeName='" + accountTypeName + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
